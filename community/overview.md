---
layout: post
title: Community Engagement and Development
categories: community overview
---

OCCAM is a community-led endeavor, designed to push forward the standard for computer architecture studies and research. As such, it is run and organized by the community, which decides all OCCAM policies and best practices. [Workshops and events](/community/events.html) are held periodically to engage the communities of academic, government, and industry researchers in discussions focused on achieving the project's goals.

OCCAM also relies upon the community to contribute [educational materials](/education/overview.html) (e.g., tutorials) and [software infrastructure](/infrastructure/overview.html) such as simulators and benchmark programs.

Please consider [taking our survey](/community/survey.html) to help shape OCCAM into a valuable, reliable, and attractive resource for computer architecture researchers.
