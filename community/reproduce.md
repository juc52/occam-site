---
layout: post
title: REPRODUCE
categories: community events
---

## Workshop on Reproducible Research Methodologies
### Saturday 15 February 2014; Orlando, Florida, USA — at [HPCA 2014](http://hpca20.ece.ufl.edu/)
(morning, room Blue Spring II)

## Introduction

Computer science and engineering research fields increasingly rely on numerous ad hoc methods to explore research breakthroughs, particularly during empirical and statistical analysis, modeling, optimization and simulation of complex computer systems.  These ad hoc methods are utilized due to a variety of factors including problem complexity and size, speed of advancement and return on investment, cost of designing prototypes, and minimal access to state-of-the-art fabrication.  However, lack of a common experimental methodology, and lack of simple and unified mechanisms, tools and repositories to preserve and exchange the whole experimental setup including all past research artifacts makes it excessively challenging or even impossible to accurately reproduce experimental results for evaluation and future advancement.

This workshop is an interdisciplinary forum for academic and industrial researchers, practitioners and developers in computer engineering to discuss ideas, experience, trustable and reproducible research methodologies, practical techniques, tools and repositories. Detailed topics of interest include but are not limited to:

<table style="margin: 0 auto;">
<tr>
<td>
<ul>
<li>Techniques for experimental reproducibility</li>
<li>Fidelity validation of experiments and tools</li>
<li>Reproducible benchmarking methodologies</li>
<li>Scientific method for computing experiments</li>
<li>Techniques to effectively leverage emulation</li>
<li>Scalable HPC computing in experimentation</li>
<li>Metrics quantifying performance vs. fidelity</li>
<li>Reproducible and extensible methodologies</li>
<li>Data-mining, crowdsourcing analysis methods</li>
</ul>
</td>
<td>
<ul>
<li>Interdisciplinary experimental methods</li>
<li>Tool, benchmark, experiment repositories</li>
<li>Program and architecture auto-tuning</li>
<li>Reproducible simulation environments</li>
<li>Experimental documentation methods</li>
<li>Techniques to preserve/share experiments</li>
<li>Methods to effectively analyze experiments</li>
<li>Extracting metadata from experiments</li>
<li>Methods for protecting intellectual property</li>
</ul>
</td>
</tr>
</table>

## Program [[PDF](/slides/Reproduce-Program.pdf)]
**8:30am: REPRODUCE Opening Remarks**<br/>Alex Jones - University of Pittsburgh

**9:00am: PROB: A tool for Tracking Provenance and Reproducibility of Big Data Experiments.** [[paper](/papers/reproduce/reproduce14_paper_01.pdf)|[slides](/slides/reproduce/reproduce14_slides_01.pdf)]<br/>Vlad Korolev and Anupam Joshi - University of Maryland, Baltimore County

**9:20am: Experiments and Analytics for Software-Hardware Optimization** [[paper](/papers/reproduce/reproduce14_paper_02.pdf)]<br/>Kingsum Chow, Pranita Maldikar, Robert Scott, Peng-Fei Chuang and Khun Ban - Intel Corporation

**9:40am: Crystal: a Design-Time Resource Partitioning Method for Hybrid Main Memory** [[paper](/papers/reproduce/reproduce14_paper_03.pdf)|[slides](/slides/reproduce/reproduce14_slides_03.pdf)]<br/>Dmitry Knyaginin, Per Stenstrom and Georgi Gaydadjiev - Chalmers University of Technology

**10:00am: Coffee break**

**10:30am: Pinballs: Portable and Shareable User-level Checkpoints for Reproducible Analysis and Simulation** [[paper](/papers/reproduce/reproduce14_paper_04.pdf)|[slides](/slides/reproduce/reproduce14_slides_04.pdf)]<br/>Harish Patil and Trevor Carlson - Ghent University

**10:50am: Efficient, Accurate and Reproducible Simulation of Multi-Threaded Workloads** [[paper](/papers/reproduce/reproduce14_paper_05.pdf)|[slides](/slides/reproduce/reproduce14_slides_05.pdf)]<br/>Trevor Carlson, Wim Heirman, Harish Patil and Lieven Eeckhout - Ghent University

**11:10am: How Much Does Memory Layout Impact Performance? A Wide Study** [[paper](/papers/reproduce/reproduce14_paper_06.pdf)|[slides](/slides/reproduce/reproduce14_slides_06.pdf)]<br/>Jean-Christophe Petkovich, Augusto Born de Oliveira and Sebastian Fischemeister - University of Waterloo

**11:30am: Hardware Generation Languages as a Foundation for Credible, Reproducible, and Productive Research Methodologies** [[paper](/papers/reproduce/reproduce14_paper_07.pdf)]<br/>Derek Lockhart and Christopher Batten - Cornell University

**11:50am: Closing Remarks** [[slides](/slides/reproduce/Z-Closing.pdf)]<br>Alex Jones - University of Pittsburgh

## Call for Papers
Submissions may consist of 2 page extended abstracts for ongoing research, wild and crazy ideas, etc., or longer publications (up to 6 pages) with more mature content and possibly examples of reproducible experiments.  Submissions should be made via [EasyChair here](https://www.easychair.org/conferences/?conf=reproduce14).

* **Submission Deadline**: <del>Wednesday 8</del> Sunday 12 January 2014 (anywhere in the world) <span style='color:red !important;'>(now passed)</span>
* **Notification of Acceptance**: Thursday 23 January 2014
* **Final Manuscripts Due**: Thursday 30 January 2014
* **Workshop Date**: Saturday 15 February 2014 at HPCA 2014 (Room Blue Spring II)

This call is a potential preliminary venue for feedback for researchers intending to submit to an [IEEE Transactions on Emerging Topics in Computing (TETC)](http://www.computer.org/portal/web/tetc) Special Issue on Reproducible Research Methodologies, which is planned for publication in 2015.  For more information, see the TETC call for papers; submissions are due Monday 1 September 2014.

## Organizers

* [Bruce Childers](http://people.cs.pitt.edu/~childers/); University of Pittsburgh; Pittsburgh, Pennsylvania, USA
* [Grigori Fursin](http://ctuning.org/lab/people/gfursin/); INRIA; Saclay, France
* [Alex Jones](http://www.engineering.pitt.edu/alex_jones/); University of Pittsburgh; Pittsburgh, Pennsylvania, USA
* [Daniel Mossé](http://people.cs.pitt.edu/~mosse/); University of Pittsburgh; Pittsburgh, Pennsylvania, USA
