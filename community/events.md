---
layout: post
title: Events and Workshops
categories: community events
---

The OCCAM portal has grown out of the need for interoperability between simulation tools in the realms of architecture simulation, hardware emulation, analytic modeling, and benchmarking infrastructure.  To kick-start the effort, a series of workshops and events were held in 2012 which engaged the communities of academic, government, and industry researchers in discussions focused on assessing the state of the architecture simulation framework, determining the needs of the computer architecture simulator community, and developing community-building strategies to sustain the effort in the long-term.

For slides from talks on OCCAM concepts and philosophy, click here.

## Upcoming Events

### ISCA Tutorial

**June 14th, 2015** ISCA: Our tutorial will demonstrate that OCCAM is a complete infrastructure for end-to-end research from hypothesis through experimentation and finally visualization. After the tutorial, participants will be able to immediately make use of and collaborate within the system for their own purposes. Participants will also have the knowledge to set up their own instances of OCCAM, which is freely and publicly available, where they can help curate tools, simulators, and research workloads within our distributed and collaborative system.

[for more information...](/community/isca-2015-occam-tutorial.html)

### CPSWeek 2015 Panel on Artifact Evaluation

**April 13th, 2015** CPSWeek: To attempt to bring openness, accountability, comparability, and repeatability to papers, specially for practical papers that use tools/artifacts, this BoF session invites the CPS community to discuss Artifact Evaluation (AE) for CPS.

[for more information...](/community/cpsweek-2015.html)

## Past Events

* **7 February 2015**: [OCCAM Tutorial](/community/hpca-2015-occam-tutorial.html) at **[HPCA-2015](http://darksilicon.org/hpca/)**; Orlando, Florida, USA
* **15 February 2014**: [Workshop on Reproducible Research Methodologies](/community/reproduce.html) at **[HPCA-2014](http://hpca20.ece.ufl.edu/)**; Orlando, Florida, USA
* **21 November 2013**: [Computer Architecture Repositories for Open Simulation and Modeling Tools Birds-of-a-Feather event](http://sc13.supercomputing.org/schedule/event_detail.php?evid=bof209) at **[SC13](http://sc13.supercomputing.org/)**; Denver, Colorado, USA
* **25 June 2013**: Community Session on OCCAM Repository and Governance at **[ISCA 2013](http://isca2013.eew.technion.ac.il/)**; Tel Aviv, Israel
* **2 May 2013**: Keynote at **[HiPEAC Computing Systems Week](http://www.hipeac.net/csw/paris13)** thematic session on ["Making computer engineering a science"](http://www.hipeac.net/thematic-session/making-computer-engineering-science); Paris, France — [[slides](/slides/hipeac2013_occam-talk.pdf)]
* **2 December 2012**: [Task and Activity Group](http://csa.cs.pitt.edu/workinggroup.html) at **[MICRO-45](http://www.microsymposia.org/micro45/)**; Vancouver, British Columbia, Canada
* **14 November 2012**: [Architecture and Systems Simulators Birds-of-a-Feather event](http://sc12.supercomputing.org/schedule/event_detail.php?evid=bof120) at **[SC12](http://sc12.supercomputing.org/)**; Salt Lake City, Utah, USA
* **31 May – 1 June 2012**: [**CSA 2012**: Workshop on a Community Supported Computer Architecture Design and Evaluation Framework](http://csa.cs.pitt.edu/); Arlington, Virginia, USA

