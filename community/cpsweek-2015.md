---
layout: post
title: CPSWeek 2015 BoF Panel on Artifact Evaluation
categories: community events
---

At: [CPSWeek 2015](http://www.cpsweek.org/2015/)

Date: **April 13th, 2015**

Time: **during the evening reception**

## A Discussion on Artifact Evaluation

To attempt to bring openness, accountability, comparability, and repeatability to papers, specially for practical papers that use tools/artifacts, this BoF session invites the CPS community to discuss Artifact Evaluation (AE) for CPS. AE has been proposed and adopted by some computing conferences/communities (see [http://www.artifact-eval.org](http://www.artifact-eval.org)) as a way to validate the software and experiments used behind the results that appear in a paper. Papers that meet a community-determined threshold for validation are awarded some validation, such as a *"award stamp"*.

The discussion of AE in the BOF will revolve around issues that include: 

* when to do AE (after or concurrently with paper review), how to do AE (what are the criteria for validation), and who does AE (part of the program committee, or a separate committee),
* how the infrastructure provides for uploading code, searching artifacts, creating benchmarks, run experiments,
* how to store and who has access to experiments and results, and (d) how to reward/incentivize researchers to participate in AE.

