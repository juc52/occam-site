---
layout: post
title: Education
categories: education overview
---

One of the central pillars of the OCCAM Project is to provide education to those doing computer architecture research and design. We will provide educational materials to cover all aspects of computer architecture, including simulator usage, experiment design, best practices, and repeatability.
