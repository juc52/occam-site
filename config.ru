require 'rack'
require 'yaml'

use Rack::Static,
  :urls => ["/"],
  :root => "_site",
  :index => "index.html"

run lambda { |env|
  [
    200,
    {
      'Content-Type'  => 'text/html'
    },
    File.open('_site/index.html', File::RDONLY)
  ]
}
